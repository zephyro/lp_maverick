
$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn_close" href="javascript:;"></a>'
    }
});

// ----- Маска ----------
jQuery(function($){
    $("input[name='phone']").mask("+7(999) 999-9999");
});




$('.btn-scroll').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500, {offset: 0 });
    return false;
});




// tabs

$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$(document).ready(function() {

    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                type    =     $('input[name="type"]', $form).val(),
                name    =     $('input[name="name"]', $form).val(),
                phone   =     $('input[name="phone"]', $form).val(),
                email   =     $('input[name="email"]', $form).val(),
                company =     $('input[name="company"]', $form).val(),
                city    =     $('input[name="city"]', $form).val();
            console.log(name, phone, type, email, city, company);
            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {name: name, phone: phone, type:type, email: email, company: company, city: city}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                document.location.href = "./done.html";
            });
        }
    });
});
